import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os

relative_path = os.path.dirname(__file__)

CSC_data = pd.read_csv(relative_path + '/CSCTimes.csv')
CSR_data = pd.read_csv(relative_path + '/CSRTimes.csv')
Dense_data = pd.read_csv(relative_path + '/DenseTimes.csv')

CSC_data

