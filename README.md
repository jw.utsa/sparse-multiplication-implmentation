# Sparse Multiplication Implmentation

### Build Info
1) Using Linux, create terminal in this repo
2) run in terminal 

    `g++ -Ofast -march=native -flto sparse_mult.cpp -o sparse_mult`

Note: using `-march=native` will create an executable optimized for your processor so may not work on others

