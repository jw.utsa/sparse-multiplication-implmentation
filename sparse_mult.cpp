#include <vector>
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <random>
#include <chrono>
#include <fstream>
#include <thread>

typedef std::vector<std::vector<double>> doubleMatrix;
typedef std::vector<double> vector;

struct sparseVector {
    std::string format;
    size_t columnAmount;
    vector data;
    vector JA;
    vector IA;
};

void printMat(doubleMatrix matrix) {
    for (int i = 0; i < matrix.size(); i++) {
        std::cout << "[";
        for (int j = 0; j < matrix[0].size(); j++) {
            std::cout << matrix[i][j] << ", ";
        }
        std::cout << "]\n";
    }
}

doubleMatrix genMatrix(doubleMatrix matrix, double percentage) {
    // matrix must be init to zeros

    srand((unsigned) time(NULL));
    auto rng = std::default_random_engine {};
    int amount = matrix.size() * matrix[0].size() * percentage;

    for (int i = 0; i < amount; i++) {
        int column_index = rand()%matrix.size();
        int row_index = rand()%matrix[column_index].size();
        if (matrix[column_index][row_index] == 0) {
            matrix[column_index][row_index] = rand()/1e6;
        } else {
            amount++;
        }
    }

    return matrix;
}

sparseVector genCSR(doubleMatrix matrix) {
    vector data;
    vector JA;
    vector IA(1);

    sparseVector output;

    int dataCounter;
    for (int i = 0; i < matrix.size(); i++) {
        dataCounter = 0;
        for (int j = 0; j < matrix[i].size(); j++) {
            if (matrix[i][j] != 0) {
                data.push_back(matrix[i][j]);
                JA.push_back(j);
                dataCounter++;
            }
        }
        if (i == 0) {
            IA.push_back(dataCounter);
        }
        else {
            IA.push_back(IA.back() + dataCounter);
        }
        
    }
    output.data = data;
    output.JA = JA;
    output.IA = IA;
    output.format = "CSR";
    output.columnAmount = matrix[0].size();

    return output;
}

sparseVector genCSC(doubleMatrix matrix) {
    vector data;
    vector JA;
    vector IA(1);

    sparseVector output;

    int dataCounter;
    for (int i = 0; i < matrix[0].size(); i++) {
        dataCounter = 0;
        for (int j = 0; j < matrix.size(); j++) {
            if (matrix[j][i] != 0) {
                data.push_back(matrix[j][i]);
                JA.push_back(j);
                dataCounter++;
            }
        }
        if (i == 0) {
            IA.push_back(dataCounter);
        }
        else {
            IA.push_back(IA.back() + dataCounter);
        }
    }

    output.data = data;
    output.JA = JA;
    output.IA = IA;
    output.format = "CSC";
    output.columnAmount = matrix[0].size();

    return output;
}

sparseVector genCOO(doubleMatrix matrix) {
    vector data;
    vector JA;
    vector IA;

    sparseVector output;

    for (int i = 0; i < matrix.size(); i++) {
        for (int j = 0; j < matrix[i].size(); j++) {
            if (matrix[i][j] != 0) {
                data.push_back(matrix[i][j]);
                JA.push_back(i);
                IA.push_back(j);
            }
        }
    }

    output.data = data;
    output.JA = JA;
    output.IA = IA;
    output.format = "COO";
    output.columnAmount = matrix[0].size();

    return output;
}

doubleMatrix denseMult(doubleMatrix matrix1, doubleMatrix matrix2) {
    doubleMatrix output(matrix1.size(), vector(matrix2[0].size()));
    double buffer;
    if (matrix1.size() == matrix2[0].size()) {
        for (int row = 0; row < matrix1.size(); row++) {
            for (int column = 0; column < matrix2[0].size(); column++) {
                buffer = 0;
                for (int i = 0; i < matrix2.size(); i++) {
                    buffer += matrix1[row][i] * matrix2[i][column];
                }
                output[row][column] = buffer;
            }
        }
    } else {
        std::cout << "wrong matrix size";
    }
    return output;
}

doubleMatrix denseSparseMult(doubleMatrix matrix, sparseVector sparseMat) {
    int amountOfOps;
    double* JA;
    double* data;
    double* IA;

    doubleMatrix output(matrix.size(), vector(sparseMat.columnAmount));

    if (sparseMat.format == "CSC") {
        double buffer;
        for (int rowDense = 0; rowDense < matrix.size(); rowDense++) {
            JA = &sparseMat.JA.front();
            data = &sparseMat.data.front();
            IA = &sparseMat.IA.front();
            for (int column = 0; column < sparseMat.IA.size()-1; column++) {
                amountOfOps = *(IA+1) - *(IA++);
                for (int multOp = 0; multOp < amountOfOps; multOp++) {
                    output[rowDense][column] += matrix[rowDense][*(JA++)] * (*(data++));
                }
            }
        }
    }
    else if (sparseMat.format == "CSR") {
        for (int rowDense = 0; rowDense < matrix.size(); rowDense++) {
            JA = &sparseMat.JA.front();
            data = &sparseMat.data.front();
            IA = &sparseMat.IA.front();
            for (int rowSparse = 0; rowSparse < sparseMat.IA.size()-1; rowSparse++) {
                amountOfOps = *(IA+1) - *(IA++);
                for (int multOp = 0; multOp < amountOfOps; multOp++) {
                    output[rowDense][*(JA++)] += *(data++) * matrix[rowDense][rowSparse];
                }
            }
        }
    }
    else if (sparseMat.format == "COO") {
        for (int rowDense = 0; rowDense < matrix.size(); rowDense++) {
            JA = &sparseMat.JA.front();
            data = &sparseMat.data.front();
            IA = &sparseMat.IA.front();
            for (int dataIndex = 0; dataIndex < sparseMat.data.size(); dataIndex++) {
                output[rowDense][*(IA++)] += *(data++) * matrix[*(JA++)][dataIndex];
            }
        }
    }

    return output;
}

void threadedPercent(
    int percentIndex,
    vector sparsePercents,
    std::vector<int> columnLength,
    std::vector<int> rowLength
) {
    using picoseconds = std::chrono::duration<long long, std::pico>;
    constexpr auto N = 10;



    vector denseTimes;
    vector cscTimes;
    vector csrTimes;

    auto sparsePercent = sparsePercents[percentIndex];
    std::cout << "Start percent " << sparsePercent << "\n";

    for (int loopIter = 0; loopIter < columnLength.size(); loopIter++) {
        std::cout << "\tLoop iteration " << loopIter << "\n";
        doubleMatrix denseMatA(rowLength[loopIter], vector(columnLength[loopIter]));
        doubleMatrix denseMatB(columnLength[loopIter], vector(rowLength[loopIter]));

        auto sparseMatA = genMatrix(denseMatA, sparsePercent);
        auto sparseMatB = genMatrix(denseMatB, sparsePercent);

        denseMatA = genMatrix(denseMatA, 1);
        denseMatB = genMatrix(denseMatB, 1);

        // auto MatCsrA = genCSR(sparseMatA);
        // auto MatCscA = genCSC(sparseMatA);
        // auto MatCooA = genCOO(sparseMatA);

        auto MatCsrB = genCSR(sparseMatB);
        auto MatCscB = genCSC(sparseMatB);
        auto MatCooB = genCOO(sparseMatB);

        doubleMatrix AB;
        doubleMatrix CSCMult;
        doubleMatrix CSRMult;

        auto t0 = std::chrono::steady_clock::now();
        for (int repeat = 0; repeat < N; ++repeat)
        {
            AB = denseMult(denseMatA, sparseMatB);
        }
        auto t1 = std::chrono::steady_clock::now();
        auto denseTime = picoseconds{t1 - t0}/N;
        denseTimes.push_back(denseTime.count());

        t0 = std::chrono::steady_clock::now();
        for (int repeat = 0; repeat < N; ++repeat)
        {
            CSCMult = denseSparseMult(denseMatA, MatCscB);
        }
        t1 = std::chrono::steady_clock::now();
        auto CSCTime = picoseconds{t1 - t0}/N;
        cscTimes.push_back(CSCTime.count());

        t0 = std::chrono::steady_clock::now();
        for (int repeat = 0; repeat < N; ++repeat)
        {
            CSRMult = denseSparseMult(denseMatA, MatCsrB);
        }
        t1 = std::chrono::steady_clock::now();
        auto CSRTime = picoseconds{t1 - t0}/N;
        csrTimes.push_back(CSRTime.count());

        std::cout << "\tFinish Loop iteration " << loopIter << "\n";
    }
    std::string cscFileName = "CSCTimesMore";
    std::string csrFileName = "CSRTimesMore";
    std::string denseFileName = "DenseTimesMore";
    std::ofstream cscFile;
    std::ofstream csrFile;
    std::ofstream denseFile;

    cscFile.open(cscFileName.append(std::to_string(percentIndex)).append(".csv"));
    csrFile.open(csrFileName.append(std::to_string(percentIndex)).append(".csv"));
    denseFile.open(denseFileName.append(std::to_string(percentIndex)).append(".csv"));

    cscFile << "Row_Amount, Column_Amount, Sparse_Percentage, Avg_Time\n";
    csrFile << "Row_Amount, Column_Amount, Sparse_Percentage, Avg_Time\n";
    denseFile << "Row_Amount, Column_Amount, Sparse_Percentage, Avg_Time\n";

    cscFile << "Row_Amount, Column_Amount, Sparse_Percentage, Avg_Time\n";
    csrFile << "Row_Amount, Column_Amount, Sparse_Percentage, Avg_Time\n";
    denseFile << "Row_Amount, Column_Amount, Sparse_Percentage, Avg_Time\n";

    for (int i = 0; i < cscTimes.size(); i++) {
        cscFile << rowLength[i] << ", " << columnLength[i] << ", " << sparsePercent << ", " << cscTimes[i] << "\n";
    }

    for (int i = 0; i < csrTimes.size(); i++) {
        csrFile << rowLength[i] << ", " << columnLength[i] << ", " << sparsePercent << ", " << csrTimes[i] << "\n";
    }

    for (int i = 0; i < denseTimes.size(); i++) {
        denseFile << rowLength[i] << ", " << columnLength[i] << ", " << sparsePercent << ", " << denseTimes[i] << "\n";
    }

    cscFile.close();
    csrFile.close();
    denseFile.close();
}

int main() {

    std::vector<int> columnLength{5, 50, 500, 5000, 3, 30, 300, 3000, 5, 50, 500, 5000};
    std::vector<int> rowLength{5, 50, 500, 5000, 5, 50, 500, 5000, 3, 30, 300, 3000};

    std::vector<double> sparsePercents{.25, .2, .15, .1, .05, .01, .001};

    std::vector<std::thread> threadVector;

    for (int percentIndex = 0; percentIndex < sparsePercents.size(); percentIndex++) {
        threadVector.push_back(std::thread(threadedPercent, percentIndex, sparsePercents, columnLength, rowLength));
    }

    for (std::thread & th : threadVector) {
        if (th.joinable()) {
            th.join();
        }
    }

    return 0;
}
